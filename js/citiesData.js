export const citiesData = [{
    lat: '52.52',
    long: '13.41',
    name: 'berlin'
},

{
    lat: '51.5002',
    long: '-0.1262',
    name: 'london'
},

{
    lat: '55.7558',
    long: '37.6176',
    name: 'moscow'
},

{
    lat: '35.7061',
    long: '51.4358',
    name: 'tehran'
},

{
    lat: '-8.8159',
    long: '13.2306',
    name: 'luanda'
},
]