const temperature = document.querySelector('#temp');
const weatherType = document.querySelector('#weatherType');
const weatherIcon = document.querySelector('#weatherIcon');
const windSpead = document.querySelector('#wind');
const findCityInput = document.querySelector('#findCityInput');
const findCityBtn = document.querySelector('#findCityBtn');
const weatherApp = document.querySelector('.weather-app');
const findCityForm = document.querySelector('#findCityForm');
const cityNameTag = document.querySelector('#cityName');
const closeAlertBtn = document.querySelector('#closeAlertBtn');
const errorWrapper = document.querySelector('#errorWrapper');
const time = document.querySelector('#time');


import {
    citiesData
} from "./citiesData.js";

// initialize 
getWeather(35.7061,51.4358,'tehran');


findCityForm.addEventListener('submit', (e) => {
    e.preventDefault();
    let cityName = findCityInput.value;
    findLatAndLong(cityName);
    findCityInput.value = '';
})

closeAlertBtn.addEventListener('click', () => {
    errorWrapper.style.display = 'none';
});

errorWrapper.addEventListener('click', (e) => {
    e.target.style.display = 'none';
})

function checkCookie() {
    let cityName = getCookie('cityName');
    if (cityName) {
        cityNameTag.innerHTML = cityName;
    }else{
        getWeather(35.7061,51.4358,'tehran');
    }
}
function setCookie(cName,cValue,cMinutes) {
    let myTime = new Date();
    myTime.setTime(myTime.getTime() + (cMinutes * 60 * 1000));
    let expires = 'expires' + myTime.toUTCString();
    document.cookie = cName + '=' + cValue + ';' + expires + ';path=/';
}

function getCookie(cName) {
    let name = cName + '=';
    let deCodedCookie = decodeURIComponent(document.cookie);
    let myCookies = deCodedCookie.split(';');

    for (let i = 0; i < myCookies.length; i++) {
        let c = myCookies[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length,c.length);
        }
    }
    return '';
}

async function getWeather(lat, long,name) {
    let response = await fetch(`https://api.open-meteo.com/v1/forecast?latitude=${lat}&longitude=${long}&current_weather=true&timeformat=unixtime`);
    let data = await response.json();
    console.log(data);
    
    // time ( ... minutes ago)
    calculateTime(data.current_weather.time);
    
    // city name
    setCookie('cityName',name,20);
    cityNameTag.innerHTML = name;

    //weather status
    let weatherStatus = showWeatherType(data.current_weather.weathercode);
    showWeatherIcon(weatherStatus);

    //temperature
    temperature.innerHTML = data.current_weather.temperature;

    //wknd spead
    windSpead.innerHTML = data.current_weather.windspeed;

}

function calculateTime(pastTime) {
    const date = new Date();
    const timeStamp = date.getTime();

    const difference = timeStamp - (pastTime*1000);
    const minutes = parseInt(difference / 60000);

    time.innerHTML = minutes;
}

function findLatAndLong(cityName) {
    let foundedCity = citiesData.find(city => {
        return city.name === cityName;
    });
    if (foundedCity === undefined) {
        errorWrapper.style.display = 'flex';
        findCityInput.value = '';
    }
    getWeather(foundedCity.lat, foundedCity.long, foundedCity.name);
}

function showWeatherType(code) {
    let weatherType = '';

    //reset style classes
    weatherApp.removeAttribute('class');
    
    // adding main style class
    weatherApp.setAttribute('class', 'weather-app');

    switch (code) {
        case 0:
            weatherType = 'sunny';
            weatherApp.classList.add('sunny-styles')
            break;
        case 1:
            weatherType = 'partly cloudy';
            weatherApp.classList.add('partly-cloudy-styles')
            break;
        case 2:
            weatherType = 'partly cloudy';
            weatherApp.classList.add('partly-cloudy-styles')
            break;
        case 3:
            weatherType = 'cloudy';
            // changeStyles('$cloudyBackGround','$cloudyInput','$cloudyButton');
            weatherApp.classList.add('cloudy-styles')
            break;
        case 61:
            weatherType = 'rainy';
            weatherApp.classList.add('rainy-styles')
            break;
        case 63:
            weatherType = 'rainy';
            weatherApp.classList.add('rainy-styles')
            break
        case 65:
            weatherType = 'rainy';
            weatherApp.classList.add('rainy-styles')
            break;
        case 71:
            weatherType = 'snowy';
            weatherApp.classList.add('snowy-styles')
            break;
        case 73:
            weatherType = 'snowy';
            weatherApp.classList.add('snowy-styles')
            break;
        case 75:
            weatherType = 'snowy';
            weatherApp.classList.add('snowy-styles')
            break;
        case 77:
            weatherType = 'snowy';
            weatherApp.classList.add('snowy-styles')
            break;
        default:
            weatherType = 'unsupported';
            break;
    }
    return weatherType;
}

function showWeatherIcon(weatherStatus) {

    if (weatherStatus !== 'unsupported') {
        weatherIcon.src = `../assets/icons/${weatherStatus}.png`;
        weatherType.innerHTML = weatherStatus;
    } 
    if(weatherStatus === 'unsupported') {
        alert('invalid city !!!');
    }
}

// function changeStyles(background,input,button) {

// }
